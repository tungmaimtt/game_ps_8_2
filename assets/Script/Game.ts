// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(sp.Skeleton)
    police: sp.Skeleton = null

    @property(sp.Skeleton)
    lupin: sp.Skeleton = null

    @property(cc.Node)
    shadow: cc.Node = null

    @property(cc.Node)
    fail: cc.Node = null

    @property(cc.Node)
    congrat: cc.Node = null

    @property(cc.Node)
    cockroach: cc.Node = null

    @property(cc.AudioClip)
    click: cc.AudioClip = null

    @property(cc.AudioClip)
    crawling: cc.AudioClip = null

    @property(cc.AudioClip)
    ding: cc.AudioClip = null

    @property(cc.AudioClip)
    hit: cc.AudioClip = null

    @property(cc.AudioClip)
    nunchaku: cc.AudioClip = null

    @property(cc.AudioClip)
    scream: cc.AudioClip = null

    @property(cc.AudioClip)
    stageFail: cc.AudioClip = null

    @property(cc.AudioClip)
    stageSuccess: cc.AudioClip = null

    isSelected = false
    endGame = false

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.lupin.setAnimation(0, 'level_8/hide_after_cabinet', true)
        this.lupin.setAnimation(1, 'emotion/sinister', true)

        this.police.setAnimation(0, 'police/level_8/sit_decorate', true)

        const optionsNode = this.node.getChildByName('Options')
        optionsNode.setScale(0)
        cc.tween(optionsNode).delay(2).to(.3, {scale: 1}).start()
    }

    tweenSelectOpt (name, cb = (option) => {}) {
        const nodeOpt = this.node.getChildByName('Options').getChildByName(name)
        cc.tween(nodeOpt)
            .by(.05, {scale: -.13})
            .by(.05, {scale: .13})
            .call(() => {
                cb(nodeOpt)
            })
            .start()
    }

    fadeScene (preCall, endCall) {
        this.shadow.active = true
        cc.tween(this.shadow)
            .to(.5, {opacity: 255})
            .call(() => {
                preCall()
            })
            .to(.5, {opacity: 0})
            .call(() => {
                this.shadow.active = false
                endCall()
            })
            .start()
    }

    darkOption (option) {
        option.getChildByName('Overlay').active = true
    }

    runOption1 () {
        if (this.isSelected) {
            return
        }

        this.isSelected = true
        cc.audioEngine.play(this.click, false, 1)

        this.tweenSelectOpt('Option1', (option) => {
            this.fadeScene(
                () => {
                    this.darkOption(option)

                    this.lupin.setAnimation(0, 'level_8/cockroach_throw', false)
                    this.lupin.setAnimation(1, 'emotion/sinister', false)

                    let aCount = 0

                    this.lupin.setCompleteListener((track) => {
                        const animationName = track.animation.name

                        if (animationName === 'level_8/cockroach_throw') {
                            this.lupin.node.scaleX *= -1
                            this.lupin.setAnimation(0, 'general/stand_thinking', false)
                            this.lupin.setAnimation(1, 'emotion/excited', true)
                        }

                        if (animationName === 'general/win_2.1') {
                            aCount++

                            if (aCount === 2) {
                                this.showCongrat(option)
                                this.lupin.paused = true
                            }
                        }
                    })

                    this.police.setMix('police/level_8/sit_decorate', 'police/level_8/nervous', .3)

                    cc.tween(this.cockroach)
                        .delay(1.6)
                        .call(() => {                            
                            this.cockroach.active = true
                        })
                        .bezierTo(
                            1,
                            cc.v2(110, -80),
                            cc.v2(50, -80),
                            cc.v2(-62, -300),
                        )
                        .delay(1)
                        .by(0, {angle: 95})
                        .to(2, {position: cc.v3(-41, 13)})
                        .delay(1)
                        .call(() => {
                            this.police.setAnimation(0, 'police/level_8/nervous', false)
                            cc.audioEngine.play(this.scream, false, 1)

                            this.police.setCompleteListener((track) => {
                                const animationName = track.animation.name

                                if (animationName === 'police/level_8/nervous') {
                                    this.lupin.setAnimation(0, 'general/win_2.1', true)
                                    this.lupin.setAnimation(1, 'emotion/idle', true)
                                }
                            })
                        })
                        .start()
                },
                () => {}
            )
        })
    }

    runOption2 () {
        if (this.isSelected) {
            return
        }

        this.isSelected = true
        cc.audioEngine.play(this.click, false, 1)

        this.tweenSelectOpt('Option2', (option) => {
            this.fadeScene(
                () => {
                    this.darkOption(option)
                    this.lupinPreWalk()
                },
                () => {
                    this.lupinWalk(() => {
                        this.lupin.setAnimation(0, 'level_8/nunchaku_', true)
                        this.lupin.setAnimation(1, 'emotion/sinister', true)
                        this.lupin.node.scaleX *= -1

                        this.police.setMix('police/level_8/sit_surprised', 'police/level_8/sit_nervous', .3)
                        this.police.setMix('police/level_8/sit_nervous', 'police/level_8/sit_decorate', .3)

                        let aCount = 0

                        cc.tween(this.police.node)
                            .delay(3.5)
                            .call(() => {
                                this.police.setAnimation(0, 'police/level_8/sit_surprised', true)
                            })
                            .start()

                        this.lupin.setCompleteListener((track) => {
                            const animationName = track.animation.name

                            if (animationName === 'level_8/nunchaku_') {
                                aCount++

                                if (aCount === 1) {
                                    let audioId
                                    cc.tween(this.lupin.node)
                                        .delay(1)
                                        .call(() => {
                                            this.lupin.setAnimation(0, 'general/nunchaku2', true)
                                            audioId = cc.audioEngine.play(this.nunchaku, true, 1)
                                        })
                                        .delay(1.5)
                                        .call(() => {
                                            this.police.setAnimation(0, 'police/level_8/sit_nervous', true)
                                        })
                                        .delay(2)
                                        .call(() => {
                                            cc.audioEngine.stop(audioId)

                                            this.lupin.setAnimation(0, 'level_8/nunchaku3', false)

                                            cc.audioEngine.play(this.hit, false, 1)

                                            cc.tween(this.lupin.node)
                                                .delay(.2)
                                                .call(() => {
                                                    this.lupin.clearTrack(1)
                                                })
                                                .delay(.5)
                                                .call(() => {
                                                    this.police.setAnimation(0, 'police/level_8/sit_decorate', false)
                                                })
                                                .delay(1)
                                                .call(() => {
                                                    this.showFail(option)
                                                })
                                                .start()
                                        })
                                        .start()
                                }
                            }
                        })
                    })
                }
            )
        })
    }

    runOption3 () {
        if (this.isSelected) {
            return
        }

        this.isSelected = true
        cc.audioEngine.play(this.click, false, 1)

        this.tweenSelectOpt('Option3', (option) => {
            this.fadeScene(
                () => {
                    this.darkOption(option)
                    this.lupinPreWalk()
                },
                () => {
                    this.lupinWalk(() => {
                        this.lupin.setMix('general/knife_attach', 'general/stand_nervous', .15)
                        this.lupin.setAnimation(0, 'general/knife_attach', false)
                        this.lupin.setAnimation(1, 'emotion/sinister', true)
                        this.lupin.node.scaleX *= -1
                        
                        this.police.setAnimation(0, 'police/level_8/sit_surprised', true)

                        this.lupin.setCompleteListener((track) => {
                            const animationName = track.animation.name

                            if (animationName === 'general/knife_attach') {
                                this.police.setAnimation(0, 'police/level_8/standup', false)
                            }

                            if (animationName === 'emotion/fear_1') {
                                this.showFail(option)
                            }
                        })

                        this.police.setCompleteListener((track) => {
                            const animationName = track.animation.name

                            if (animationName === 'police/level_8/standup') {
                                this.police.setAnimation(0, 'police/level_8/sit_kick2', false)

                                cc.tween(this.lupin.node)
                                    .delay(.575)
                                    .call(() => {
                                        this.lupin.setAnimation(0, 'general/stand_nervous', false)
                                        this.lupin.setAnimation(1, 'emotion/fear_1', false)

                                        // cc.audioEngine.play(this.hit, false, 1)
                                    })
                                    .start()
                            }

                            if (animationName === 'police/level_8/sit_kick2') {
                                this.police.setAnimation(0, 'police/level_8/stand_ready', false)
                            }
                        })
                    })
                }
            )
        })
    }

    lupinPreWalk (cb = () => {}) {
        this.lupin.setAnimation(0, 'level_8/approach_cabinet', true)
        this.lupin.clearTrack(1)

        this.lupin.node.setPosition(
            this.lupin.node.position.x - 250,
            this.lupin.node.position.y,
        )

        cb()
    }

    lupinWalk (cb = () => {}) {
        let audioId
        cc.tween(this.lupin.node)
            .to(6,
                {position: cc.v3(
                    this.lupin.node.position.x + 20,
                    this.police.node.position.y - 30,
                )}
            )
            .call(() => {
                cc.audioEngine.stop(audioId)
                cb()
            })
            .start()
    }

    showResult (option: cc.Node, result: cc.Node, sound: cc.AudioClip) {
        // show tick on option

        const overlayNode = option.getChildByName('Overlay')
        const tickNode = option.getChildByName('Tick')

        overlayNode.active = true
        tickNode.active = true

        // show result

        result.opacity = 0
        result.setScale(10)
        result.active = true

        cc.tween(result)
            .delay(.2)
            .call(() => {
                this.node.getChildByName('Options').active = false

                this.shadow.opacity = 0
                this.shadow.active = true
                cc.tween(this.shadow).to(.1, {opacity: 150}).start()
            })
            .to(.5, {opacity: 255, scale: 1})
            .call(() => {
                cc.audioEngine.play(sound, false, 1)
                this.endGame = true
            })
            .start()
    }

    showFail (option: cc.Node) {
        this.showResult(option, this.fail, this.stageFail)
    }

    showCongrat (option: cc.Node) {
        this.showResult(option, this.congrat, this.stageSuccess)
    }

    restartGame () {
        return
        const optionsNode = this.node.getChildByName('Options')

        this.fadeScene(
            () => {
                this.lupin.setAnimation(0, 'level_8/hide_after_cabinet', true)
                this.lupin.setAnimation(1, 'emotion/sinister', true)

                this.police.setAnimation(0, 'police/level_8/sit_decorate', true)

                optionsNode.setScale(0)
                optionsNode.active = true

                this.fail.active = false
                this.congrat.active = false
            },
            () => {
                cc.tween(optionsNode).to(.3, {scale: 1}).start()
            }
        )
    }

    clickAnyWhere (x) {
        if (this.endGame) {
            this.endGame = false
            this.restartGame()
        }
    }

    // update (dt) {}
}
